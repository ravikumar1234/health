package com.emids.heathTestCases;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.health.insurance.BasePremiumCalc;
import com.emids.health.insurance.HealthEntity;

public class TestCase6 {

	@Test
	public void test() {
		HealthEntity en = new HealthEntity();
		en.setAge(42);
		en.setGender("male");
		en.setHyperTension("yes");
		en.setBloodPressure("yes");
		en.setBloodSugar("no");
		en.setOverWeight("yes");
		en.setSmoking("yes");
		en.setAlcohal("yes");
		en.setDailyExercise("no");
		en.setDrugs("no");
		
		BasePremiumCalc base = new BasePremiumCalc();		
		double res=base.premiumInsuranceCalc(en);
		
		assertEquals(8161.67857866,res,0.67857866);
	}

}
