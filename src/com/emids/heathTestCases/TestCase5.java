package com.emids.heathTestCases;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.health.insurance.BasePremiumCalc;
import com.emids.health.insurance.HealthEntity;

public class TestCase5 {

	@Test
	public void test() {
		HealthEntity en = new HealthEntity();
		en.setAge(37);
		en.setGender("male");
		en.setHyperTension("yes");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("yes");
		en.setSmoking("yes");
		en.setAlcohal("yes");
		en.setDailyExercise("yes");
		en.setDrugs("no");
		
		BasePremiumCalc base = new BasePremiumCalc();		
		double res=base.premiumInsuranceCalc(en);
		
		assertEquals(7838.443783,res,0.443783);
	}

}
