package com.emids.heathTestCases;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.health.insurance.BasePremiumCalc;
import com.emids.health.insurance.HealthEntity;

public class TestCase4 {

	@Test
	public void test() {
		HealthEntity en = new HealthEntity();
		en.setAge(29);
		en.setGender("male");
		en.setHyperTension("no");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("yes");
		en.setSmoking("no");
		en.setAlcohal("yes");
		en.setDailyExercise("yes");
		en.setDrugs("no");
		
		BasePremiumCalc base = new BasePremiumCalc();		
		double res=base.premiumInsuranceCalc(en);
		
		assertEquals(6227.100560,res,0.100560);
	}

}
