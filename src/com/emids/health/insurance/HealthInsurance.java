package com.emids.health.insurance;

import java.util.Scanner;

@SuppressWarnings("resource")
public class HealthInsurance {
	
	public static void main(String[] args){
		
		
		Scanner sc=new Scanner(System.in);
		
		HealthEntity en = new HealthEntity();
		
		BasePremiumCalc baseClac = new BasePremiumCalc();
		
		System.out.println("Name:");
		String name  =sc.nextLine();
		
		System.out.println("Gender:");
		en.setGender(sc.nextLine());
		
		System.out.println("Age:");
		en.setAge(sc.nextInt());
		
		System.out.println("Current Health:");
		
		System.out.println("Hypertension:");
		en.setHyperTension(sc.next());
		
		System.out.println("Blood pressure:");
		en.setBloodPressure(sc.next());
		
		System.out.println("Blood sugar:");
		en.setBloodSugar(sc.next());
		
		System.out.println("Overweight:");
		en.setOverWeight(sc.next());
		
		System.out.println("Habits:");
		
		System.out.println("Smoking:");
		en.setSmoking(sc.next());
		
		System.out.println("Alcohol:");
		en.setAlcohal(sc.next());
		
		System.out.println("Daily Exercise:");
		en.setDailyExercise(sc.next());
		
		System.out.println("Drugs:");
		en.setDrugs(sc.next());
		
		if(en.getGender().equalsIgnoreCase("Male")){
			System.out.println("Health Insurance Premium for Mr. "+name+": Rs. "+baseClac.premiumInsuranceCalc(en));
		}else if(en.getGender().equalsIgnoreCase("Female")){
			System.out.println("Health Insurance Premium for Mrs. "+name+": Rs. "+baseClac.premiumInsuranceCalc(en));
		}else{
			System.out.println("Health Insurance Premium for "+name+": Rs. "+baseClac.premiumInsuranceCalc(en));
		}
	}
		
}
