package com.emids.health.insurance;

public class BasePremiumCalc {
	
	public double premiumInsuranceCalc(HealthEntity en){
		
		double base = 5000;
		
		if(en.getAge()<18){
			base = 5000;
		}else if(en.getAge()>17 && en.getAge()<26){
			base += (base * 10)/100;
		}else{
			base = 5500;
			int counter = 0;
			for(int i=26; i<=en.getAge();i++){
				counter++;
				if(counter==1){
					base += (base*10)/100;
					//System.out.println("base::"+base);
				}
				if(counter==5){
					counter = 0;
				}
				if(i==40){
					break;
				}
			}
			if(en.getAge()<40){
				counter = 0;
				for(int j=41; j<=en.getAge(); j++){
					counter++;
					if(counter==1){
						base += (base*20)/100;
					}
					if(counter==5){
						counter = 0;
					}
				}
			}
			
		}
		
		if(en.getGender().equalsIgnoreCase("Male")){
			base += (base*2)/100;
			//System.out.println("base M"+base);
		}
		
		if(en.getHyperTension().equalsIgnoreCase("Yes")){
			base += (base*1)/100;
			//System.out.println("base Hy"+base);
		}
		
		if(en.getBloodPressure().equalsIgnoreCase("Yes")){
			base += (base*1)/100;
			//System.out.println("base bp"+base);
		}
		
		if(en.getBloodSugar().equalsIgnoreCase("Yes")){
			base += (base*1)/100;
			//System.out.println("base bs"+base);
		}
		
		if(en.getOverWeight().equalsIgnoreCase("Yes")){
			base += (base*1)/100;
			//System.out.println("base ov"+base);
		}
		
		if(en.getSmoking().equalsIgnoreCase("Yes")){
			base += (base*3)/100;
			//System.out.println("base sm"+base);
		}
		
		if(en.getAlcohal().equalsIgnoreCase("Yes")){
			base += (base*3)/100;
			//System.out.println("base al"+base);
		}
		
		if(en.getDailyExercise().equalsIgnoreCase("Yes")){
			base -= (base*3)/100;
			//System.out.println("base dl"+base);
		}
		
		if(en.getDrugs().equalsIgnoreCase("Yes")){
			base += (base*3)/100;
			//System.out.println("base dr"+base);
		}
		
		return base;
	}

}
