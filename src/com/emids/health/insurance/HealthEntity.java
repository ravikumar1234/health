package com.emids.health.insurance;

public class HealthEntity {
	
	String gender;
	
	int age;
	
	String hyperTension;
	
	String bloodPressure;
	
	String bloodSugar;
	
	String overWeight;
	
	String smoking;
	
	String alcohal;
	
	String dailyExercise;
	
	String drugs;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}

	public String getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public String getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public String getOverWeight() {
		return overWeight;
	}

	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

	public String getSmoking() {
		return smoking;
	}

	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	public String getAlcohal() {
		return alcohal;
	}

	public void setAlcohal(String alcohal) {
		this.alcohal = alcohal;
	}

	public String getDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public String getDrugs() {
		return drugs;
	}

	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
}
